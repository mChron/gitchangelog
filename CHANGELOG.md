# Changelog


## (unreleased)

### Changes

* Trying changelog generation from the ci. [Marcus Chronabery]

### Other

* <!--[skip ci] --> chg: dev: commit from CI for changelog. [Gitlab-CI]

* <!--[skip ci] --> chg:dev:commit from CI for changelog. [Gitlab-CI]

* <!--[skip ci] --> chg:dev:commit from CI for changelog. [Gitlab-CI]

* <!--[skip ci] --> chg:dev:commit from CI for changelog. [Gitlab-CI]

* <!--[skip ci] --> chg:dev:commit from CI for changelog. [Gitlab-CI]


## v0.1.1 (2019-08-22)

### Refactors

* Example refactor message <!--@refactor.  -->Actual commit message = `example refactor message [!@]refactor` [Marcus Chronabery]

### New

* Example new message. Actual commit msg = `new: example new message.` [Marcus Chronabery]

### Changes

* Add .gitlab-ci.yml. [Marcus Chronabery]

* Example change message. Actual commit msg = `chg: example change message.` [Marcus Chronabery]

* Modified changelog config to properly identify releases, generated new changelog. [Marcus Chronabery]

### Fix

* Example new message. Actual commit msg = `fix: example new message.` [Marcus Chronabery]


## v0.1.0 (2019-08-22)

### Changes

* Modified changelog config to classify refactors. Created initial changelog. [Marcus Chronabery]


