ARG FROM_IMAGE=python
ARG FROM_VERSION=alpine3.10
FROM ${FROM_IMAGE}:${FROM_VERSION}

ARG VERSION
ARG BUILD_DATE_TIME
ARG REPO
ARG REPO_BRANCH
ARG IMAGE_TAG
ARG GIT_SHA
ARG GIT_AUTHOR
ARG PYTHON_VERSION=3.6.7
ARG PYTHON_GITCHANGELOG_VERSION=3.0.4
ARG PYTHON_PYSTACHE_VERSION=0.5.4

LABEL org.opencontainers.image.created="${BUILD_DATE_TIME}" \
  org.opencontainers.image.authors="${GIT_AUTHOR}" \
  org.opencontainers.image.url="${REPO}" \
  org.opencontainers.image.documentation="${REPO}" \
  org.opencontainers.image.source="${REPO}" \
  org.opencontainers.image.version="${REPO_BRANCH}" \
  org.opencontainers.image.revision="${GIT_SHA}" \
  org.opencontainers.image.vendor="" \
  org.opencontainers.image.licenses="" \
  org.opencontainers.image.ref.name="" \
  org.opencontainers.image.title="Services-gitchangelog" \
  org.opencontainers.image.description="Docker image for Gitchangelog - A python script to generate changelogs based on git commits" \
  org.label-schema.docker.cmd="docker run --rm -v <some_dir>:/code ${IMAGE_TAG} [-h] [--version] [--debug] [REVLIST] > CHANGELOG.md" \
  org.label-schema.docker.cmd.debug="docker exec -it --rm --entrypoint /bin/bash [CONTAINER]" \
  org.label-schema.docker.params="" \
  org.opencontainers.image.component.python.version="${PYTHON_VERSION}" \
  org.opencontainers.image.component.python.gitchangelog.version="${PYTHON_GITCHANGELOG_VERSION}" \
  org.opencontainers.image.component.python.pystache.version="${PYTHON_PYSTACHE_VERSION}"

RUN apk update && apk add git
RUN pip3 install gitchangelog==${PYTHON_GITCHANGELOG_VERSION} pystache==${PYTHON_PYSTACHE_VERSION}

RUN adduser -D -u 1000 changelog

USER changelog
WORKDIR /code

VOLUME ["/code"]

ENTRYPOINT ["gitchangelog"]
