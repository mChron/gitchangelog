# Summary
Image based on the official python alpine image. Uses the [gitchangelog python library](https://github.com/vaab/gitchangelog)

# Running The Image
*It is recommended that you integrate this into your makefile and/or gitlab-ci config to do this for you automatically.*

`docker run --rm -v /path/to/your/code:/code gitchangelog:latest > CHANGELOG.md`

# Config
*.gitchangelog.rc should be placed in the root of your git project*

Reference [.gitchangelog.rc](https://gitlab.com/mChron/gitchangelog/blob/master/.gitchangelog.rc) for configuration of changelog sections and filter rules.

# Commit Syntax
*Instructions for the syntax of your commit in order for gitchangelog to properly classify commits. See [CHANGELOG.md](https://gitlab.com/mChron/gitchangelog/blob/master/CHANGELOG.md) for examples. If you forget to add these tags to your commit you can modify the commit message by issuing `git commit --amend`*

**ACTION: [AUDIENCE:] COMMIT_MSG [!TAG ...]**

## Action
*What the change is about. Minimally include this in your commit.*

**One of chg, fix, new**

### Changes - chg
*Bumps version -.-.#*
* Refactors
* Small improvements
* Cosmetic changes

### Fixes - fix
*Bumps version -.-.#*
* Bug fixes

### New Features - New
*Bumps version #.#.-*
* New features
* Large improvements

## Audience
*Optional. Who is concerned with the change.*

**One of dev, usr, pkg, test, doc**
* **Dev** - Messages intended for developers only. Currently filters commits out of the changelog.
* ~~**Pkg** - Messages intended for packagers. Filters commits out of the changelog.~~
* ~~**Usr** - Messages intended for end users. E.g. UI, functionality change.~~
* ~~**Test** - Messages intended for testers.~~
* ~~**Doc** - Messages intended for documentation writers.~~

## Tags
*Optional. Additional filter words for the changelog.*

*When drafting your commit bash sometimes has issues with using ! in your commit message, you can try escaping it with a backslash or use @ instead.*
* **refactor** - ~~filtered out of changelog.~~ Currently filtered into it's own section.
* The following tags aren't recommended since the same filtering can be accomplished with `dev:`
 * ~~**minor** - filtered out of the changelog.~~
 * ~~**cosmetic** - filtered out of the changelog.~~
 * ~~**wip** - filtered out of the changelog.~~ Potentially used in the future.
